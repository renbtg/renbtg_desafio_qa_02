# Processo seletivo, automação de testes

Este projeto contém 3 dos 4 cenários propostos, não houve tempo fazer o 2o serviço,

## Instalando

   Este é um projeto cucumber/ruby, as gemas das quais dependemos podem ser instaladas
via `bundle install`

### Prerequisitos

Ruby >= 2.3 instalado (https://www.ruby-lang.org/en/downloads/)

** BUNDLER disponível, pode ser instalado , por exemplo, via `gem install bundler` depois da instalação do Ruby.

### Instalando gems do Ruby necessárias para testes
   Gems necessárias podem ser instaladas via `bundle install`.

# Executando testes automatizados

Para todos cenários, basta rodar `bundle exec cucumber`.

Para um cenário específico (um cenário por feature), `bundle exec cucumber features/ft/nome_da_feature.feature`


### Outputs
Outputs (Report.html, Report.json, logs e screenshots) estarão disponíveis em `reports\`


# Massas de dados

   Subdiretório `massas_feature` contém uma pasta para cada cenário, cada uma delas conténdo
um arquivo `massa.xls`.

   Apenas primeira linha com coluna Excel `UTILIZAR_MASSA` diferente de `'N'` será utilizada.

   Valores dinâmicos são obtidos com o prefixo %dyn.

   Caso desejado, podem ser feitos testes com valores hardcoded, modificando arquivo(s) `massa.xls` correspondente(s).

### Valores dinâmicos para Massas de dados nas planilhas massa.xls
   `%dyn airport_code` -> código internacional de aeroporto, 3 dígitos

   `%dyn customer_country` -> nome de país aleatório, válido para formulario ADD CUSTOMER de phptravels.net (lista de países obtida inspecionando HTML de phptravels.net)

   `%dyn password` -> senha (15 caracteres = tamanho default)

   `%dyn password 9` -> senha , 9 caracteres

   `%dyn int_rand -12 415` -> inteiro aleatório entre -12 e 415.

   `%dyn near_hour -12 36` -> datetime , 12 horas antes de agora até 36 horas depois de agora

   `%dyn near_hour -24 48 2018-12-26T15:16:17` -> datetime , de 24 horas antes a 48 horas depois da data/hora especificada (use o formato aqui descrito que nao contém espalos na data/hora)

   `%dyn fake city` -> Nome de cidade aleatório, da gema Faker

   `%dyn fake zip` -> Cep aleatório, da gema Faker

   `%dyn fake country` -> Nome de país aleatório, da gema Faker

   `%dyn fake last_name` -> Sobrenome aleatório, da gema Faker

   `%dyn fake first_name` -> 1o nome aleatório, da gema Faker

   `%dyn fake mobile` -> Número de celular nome aleatório, da gema Faker

   `%dyn fake address1` -> 1a linha de endereço (logradouro + número) aleatória , da gema Faker

   `%dyn fake address2` -> 2a linha de endereço (cidade + estado) aleatrória , da gema Faker

   `%dyn email` -> email seguro aleatório (blabla@example.bla), da gema Faker
