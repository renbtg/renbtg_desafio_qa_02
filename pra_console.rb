require 'cucumber'
require 'selenium/webdriver'
require 'capybara/dsl'
require 'site_prism'
require 'faker'
require 'monetize'

load 'features/support/shared_utils.rb'
load 'features/support/log.rb'
load 'features/support/basic_utils.rb'
load 'features/support/utils.rb'
load 'features/support/util_spreadsheet.rb'
load 'features/support/methods_of_app_life_cycle_hooks.rb'

load 'features/service/imc/ImcCreateActivity.rb'
load 'features/page/imc/ImcLoginPage.rb'
load 'features/page/imc/ImcFlightsPage.rb'
load 'features/page/imc/ImcCustomersPage.rb'



def fazer_login(url=nil)
	if $massa['QUALAPP']=='mob'
		aberturaContaDigital = AberturaContasDigital.new
		aberturaContaDigital.acessarLink url
		aberturaContaDigital.iniciarAbertura
	else
		WebdesktopAcessarAbertura.new.processa		
	end
end

def chama_alguns_passos(a)
	a.each do |i|
		write_log "chamando passo #{i}"
		chama_passo i
	end

end

def chama_passo(classe_passo)
	$numero_step_atual = classe_passo if classe_passo.class == Fixnum
	write_log "chama_passo, parametro classe_passo=#{classe_passo}"
	

	write_log "P00 classe_passo.to_s=#{classe_passo.to_s}"
	if classe_passo.is_a? Fixnum and classe_passo >= 8
		if classe_passo == 8
			(status, msg_excecao) = espera_status_veracidade
			return [status, msg_excecao]
		elsif classe_passo == 9
			pastaDigital = AcessarPastaDigital.new
			pastaDigital.acessarPasta
			pastaDigital.iniciarBusca
			pastaDigital.analiseDocumentacoes
		elsif classe_passo == 10
			tfc_faz_automacao_do_cenario
		else
			falhar 'pra_console.rb :::: chama_passo -> classe_passo numerica invalida' 
		end
	end
	write_log "P01 classe_passo.to_s=#{classe_passo.to_s}"


	if classe_passo.is_a? Fixnum
	
		if $massa['QUALAPP']=='webdesktop'
			classe_passo = case classe_passo
				when 0 then WebdesktopAcessarAbertura
				when 1 then PrimeiraParte
				when 2 then SegundaParte
				when 3 then TerceiraParte
				when 4 then QuartaParte
				when 5 then QuintaParte
				when 6 then SextaParte
				when 7 then WebdesktopEnvioDaSolicitacao
				else nil
			end
		elsif
			classe_passo = case classe_passo
				when 0 then AberturaContasDigital
				when 1 then PrimeiroPasso
				when 2 then SegundoPasso
				when 3 then TerceiroPasso
				when 4 then QuartoPasso
				when 5 then QuintoPasso
				when 6 then SextoPasso
				when 7 then EnvioDaSolicitacao
				else nil
			end
		else
			if classe_passo == 8
				(status, msg_excecao) = espera_status_veracidade
			elsif classe_passo 

			end
		end
		 
	end
	write_log "P02 classe_passo.to_s=#{classe_passo.to_s}"

	classe_passo.new.processa if classe_passo and classe_passo.class == Class 
end
