﻿# encoding: UTF-8
# TODOS requires têm que vir aqui antes, antes de IRB.start, para que o "main object" inclua as functions "globais" 
require 'irb'
require 'capybara/dsl';
include Capybara::DSL;


require_relative "features/support/utils.rb"



puts 'Antes de register_driver'

if true
	Capybara.register_driver :selenium do |app|
	  100.times {puts "capybara.rb, dentro de bloco register_driver"}
	  client = Selenium::WebDriver::Remote::Http::Default.new
  	client.timeout = 120 #2017Ago13 - visit, mais timeout: passar client em httpc_lient pra Capybara::Selenium::Driver.new 
	  retval = Capybara::Selenium::Driver.new(app, :browser => :chrome, :http_client =>client)
	  puts "register_driver, retval.class=#{retval.class}"
	  retval
	end


	Capybara.default_driver = ARGV[0].to_sym
	Capybara.javascript_driver = ARGV[0].to_sym

	Capybara.configure do |config|
		10.times {puts "capybara.rb, bloco em Capybara.configure"}
		#Capybara.current_driver = :selenium
		config.default_max_wait_time = 20 #PRA ATUALIZACOES AJAX
		
		#puts "HMMM, eu gostaria de posicionar cada uma das 4 janelas ocupando 1/4 da tela. Devo usar AUTOIT, creio!"	
		#Capybara.page.driver.manage.timeouts.page_load = 120 #2017Ago13, "visit lento nao quebrabdo?" - https://stackoverflow.com/questions/11373351/selenium-webdriver-and-wait-for-page-to-load
		#Capybara.page.driver.browser.manage.window.maximize
	end

end
	def visitar(url='https://mobhk.portal.santanderbr.pre.corp')
		visit url
	end

ARGV.clear
IRB.start # leave the console idle
