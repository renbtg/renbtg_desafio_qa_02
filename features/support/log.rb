require 'logger'
require 'FileUtils'
require_relative './utils.rb'
require_relative './shared_utils.rb'


$LOG = nil
$LOG_FNAME=nil
$LOG_SYMLEVEL=:debug #2017Set24 - REVAMPED! IMPORTANTE! FALHAVa em trace etc.

def get_log_fname
	runTpn = nil

	runScen = Thread.current[:nome_cenario_corrente] || ''
	fname = "reports/Log_#{runScen}.log"
	return fname
end
def get_log()
# TODO 2017Ago16, 13:07 - PROBLEMA: excecoes nao-tratadas nao vao pro log, acabam indo pro console.

	fname = get_log_fname()
	criadir_senaoexistir (File.dirname fname)
	if fname != $LOG_FNAME
		$LOG = Logger.new(fname, 0, 100 * 1024 * 1024 * 1024)
		$LOG_FNAME=fname
		$LOG.level = to_loglevel($LOG_SYMLEVEL)
	end


	return $LOG 
end

def to_symlevel(level)
	if level.is_a? Symbol
		retval = level
	elsif level == Logger::FATAL 
		retval = :fatal
	elsif level == Logger::ERROR
		retval = :error
	elsif level == Logger::WARN 
		retval = :warn
	elsif level == Logger::INFO
		retval = :info
	elsif level == Logger::DEBUG 
		retval = :debug
	elsif level == Logger::UNKNOWN
		retval = :unknown
	else
		fail "erro em to_symlevel(#{level}"
	end

	#puts "to_symlevel(#{level}) retornando #{retval}"
	return retval
end

def to_loglevel(sym)
	if not sym.is_a? Symbol
		retval = sym
	elsif sym == :unknown
		retval = Logger::UNKNOWN
	elsif sym == :fatal
		retval = Logger::FATAL
	elsif sym == :error
		retval = Logger::ERROR
	elsif sym == :warn
		retval = Logger::WARN
	elsif sym == :info
		retval = Logger::INFO
	elsif sym == :debug
		retval = Logger::DEBUG
	elsif sym == :trace
		retval = Logger::DEBUG
	else
		raise "erro em to_loglevel(#{sym}"
	end
	#puts "to_loglevel(#{sym}) retornando #{retval}"
	return retval
end



def write_log(p1, p2=nil)
	#protege contra chamada a write_log SEm o parametro :LEVEL inicial
	if p1.is_a? Symbol
		level=p1
		msg=p2
	else
		level=p2 || :debug
		msg=p1
	end
	
	prefixo = "#{Time.now} ,"
	#2018Abr01 - aidcionado PID= a msg_mais
	msg_mais = "PID=#{Process.pid} TPN=#{ENV['TEST_PROCESS_NUM']} de #{ENV['TEST_PROCESS_COUNT']} (PAR=#{ENV['TEST_PARALELISMO']}) #{msg}"
	full_msg="#{prefixo} => #{msg_mais}"
	
	if to_symlevel(level) == :trace and $LOG_SYMLEVEL == :debug
		#puts "hmm, ignorando msg #{to_symlevel(level)} pq $LOG_SYMLEVEL=#{$LOG_SYMLEVEL}"
		# NADA, cado de excecao, omite detalhes :trace
	else
		#puts "adicionando ao LOG , level=#{level}, to_loglevel(level)=#{to_loglevel(level)}"
		get_log.add to_loglevel(level), msg_mais
		if to_loglevel(level) >= to_loglevel($LOG_SYMLEVEL)
			STDOUT.puts full_msg #STDOUT - nao vai pra log do cucumber 
	 	else
	 		#puts "log.rb nao escreve, to_loglevel(level)=#{to_loglevel(level)}, e to_loglevel($LOG_SYMLEVEL)=#{to_loglevel($LOG_SYMLEVEL)}"
	 	end
	end

end

def write_log_debug(msg)
	write_log :debug, msg
end

def write_log_fatal(msg)
	write_log :fatal, msg
end

def write_log_info(msg)
	write_log :info, msg
end

def write_log_warn(msg)
	write_log :warn, msg
end

def write_log_unknown(msg)
	write_log :trace, msg
end

def criadir_senaoexistir(some_path)
	FileUtils.mkdir_p(some_path)
end


