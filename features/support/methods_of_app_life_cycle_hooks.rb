require "base64"
#INSTALLATION_STATE = {


def wrap_before_do(scenario)
	###Capybara.page.driver.browser.manage.window.maximize - DESNECESSARIo maximizar aqui
	$numero_step_atual = 0
	begin
		write_log :debug, "Before do de cenario com nome #{scenario.name}"

		Thread.current[:nome_cenario_corrente] = scenario.name
		begin
			set_massa_cen 
		rescue Exception => e
			#2018Abr03 - QUE FAÇO DA VIDA se falhar na leitura da massa???
			falhar e
		end
		write_log :debug, "wrap_before_do, $massa=#{$massa}"

		if ($massa['BROWSER']||'') != 'N' 
			abre_janela_browser
		end

	rescue Exception => e
   		$falhou_em_before_hook = true #Pode ser usado pra controles posteriores
  		falhar e
  	end

end

def wrap_after_do(scenario)
	write_log :debug, "wrap_after_do(scenario) - P0000 - vai chamar BLOCK EXCLUSIVO"

	
	begin
		runAfterDo(scenario)
	rescue Exception => e
		write_log :error, "wrap_after_do(scenario) - Alguma excecao em [runAfterDo], excecao=#{e}"
	end
	begin
		if ($massa['BROWSER']||'') != 'N' 
			fecha_janela_browser
		end
	rescue Exception => e
		write_log :error, "wrap_after_do(scenario) Alguma excecao em [fecha_janela_browser], excecao=#{e}"
	end

end


def runAfterDo(scenario)
	if ($massa['BROWSER']||'') != 'N' 
		screenshots_after_do
	end

	dormir_depois = 5
#		$LOG.debug "dormindo #{dormir_antes_scrshot} segundos antes do screenshot em AFTER"
	if dormir_depois and dormir_depois > 0
		write_log :debug, "runAfterDo(scenario) - dormindo #{dormir_depois} segundos depois dos screenshots. Movido de ANTES pra DEPOIS em 2017Nov17 am"
		sleep dormir_depois 
		write_log :debug, "runAfterDo(scenario) - dormiu #{dormir_depois} segundos depois dos screenshots. Movido de ANTES pra DEPOIS em 2017Nov17 am"
	end
	return
end

def screenshots_after_do
	write_log :debug, "screenshots_after_do, P00"
	browser_png = "#{get_nome_de_screenshot}/scrshot_final_report.browser.png"

	write_log :debug, "screenshots_after_do, P03"
	#2018Mar23 - adeus, sleep 3
	begin
		write_log :debug, "screenshots_after_do, P04, DE FATO fazendo screenshot final default"
		page.save_screenshot browser_png
		image = open(browser_png, 'rb') {|io|io.read}
		$LOG.debug "After do |scenario| RSI P07"
		encoded_img = Base64.encode64(image)
		embed(encoded_img, 'image/png;base64', "Screenshot")
	rescue Exception => e
		write_log :error, "screenshots_after_do - Excecao #{e} no screnshot do browser"
	end

end