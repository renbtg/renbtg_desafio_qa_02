require_relative './basic_utils.rb'
require_relative './utils.rb'
require_relative './log.rb'



def falhar(msg, definitivo=false)
	if (not msg.is_a? String)
		msg = msg.to_s
	else
		#nada
	end

	new_msg=ascii_only_str(msg) #2017Nov14, protege tambem diretamente aqui

	write_log :error, new_msg

	fail new_msg
end

def ascii_only_str(non_ascii_string)
	replacements = { 
	#  'á' => "a",
	# @ 'ë' => 'e'
	}



	encoding_options = {
	  :replace => "*",             # Use a blank for those replacements
	  :invalid   => :replace,     # Replace invalid byte sequences
	  :universal_newline => true, # Always break lines with \n
	  # For any character that isn't defined in ASCII, run this
	  # code to find out how to replace it
	  :fallback => lambda { |char|
	    # If no replacement is specified, use an empty string
	    replacements.fetch(char, "*")
	  },
	}

	ascii = non_ascii_string.encode(Encoding.find('ASCII'), encoding_options)
	#puts ascii.inspect
	return ascii
end

def remove_nonascii_exception
	#2017Out17 - TODOS entry-points de page-object devem ser protegidos por este método
	begin
		yield
	rescue Exception => e
		write_log :trace, "remove_nonascii_exception, e=#{e}, e.class=#{e.class}"
		new_text=nil
		begin
			emsg=e.message
			new_text=add_app_prefix_to_msg(ascii_only_str(emsg))
		rescue Exception => xe
			write_log :trace, "remove_nonascii_exception - Excecao #{xe} ao obter e.message"
		end
		if new_text and new_text != e.message
			raise e, new_text, e.backtrace
		else
			raise e
		end
	end
end

def escreve_xls_de_hash(xlsname,data)
	begin
		mkdir_noexist get_automtmpdir
	rescue SystemCallError => e
		#nada
	end
	f_m = nil
	nome_tmp="#{get_automtmpdir}/csv_tmp_#{obtem_guid}.txt"

	begin
		f_m = File.open(nome_tmp, "w+t")

		data.keys.each_index do |i|
			f_m.write data.keys[i]
			if i < (data.keys.length - 1)
				f_m.write ";"
			else
				f_m.write "\n"
			end
		end
		data.keys.each_index do |i|
			k=data.keys[i]
			f_m.write data[k].to_s # ASPAS simples - sugere formato
			if i < (data.keys.length - 1)
				f_m.write ";"
			else
				f_m.write "\n"
			end
		end
	rescue Exception => e
		f_m.close if f_m != nil
		raise e
	ensure
		f_m.close if f_m != nil
	end

	book = Spreadsheet::Workbook.new
	sheet1 = book.create_worksheet
	header_format = Spreadsheet::Format.new(
	  :weight => :bold,
	  :horizontal_align => :center,
	  #:bottom => true, #exemplo q vi bottom invalido
	  :locked => true
	)
	sheet1.row(0).default_format = header_format
	CSV.open(nome_tmp, 'r', :col_sep=>';') do |csv|
		csv.each_with_index do |row, i|
	    	#write_log :debug, "manipulando linha #{i}"
	    	sheet1.row(i).replace row
	  	end
	end
	FileUtils.rm nome_tmp

	book.write(xlsname)
	#book.io.close #2018Mar11 - pode resolver problemas intermitentes de nao permitir edição de XLSs criados pelo programa  #DAMN, depois de write, io fica nil. FIQUE ATENTO!
	return
end

def append_line_to_file(fname, str)
	File.open(fname,'a+') { |f| f.puts("#{str}") }
	return
end
def write_text_to_file(fname, str)
	File.open(fname,'w') { |f| f.write("#{str}") }
	return
end

def strzero(inum, nchars)
	fmtstr="%0#{nchars}d"
	return sprintf(fmtstr, inum)
end

def gera_screenshot(nomeTela=nil)
	if false
		10.times {write_log :debug, "gera_screenshot, desabilitados, nomeTela=#{nomeTela}"}
		return
	end

    sleep 4 #rbattaglia 2018Fev06 - já tinha este sleep 4. Necessario? Sempre?
	scrshot_fullpath = "#{get_nome_de_screenshot}/#{Time.now.strftime('%y%m%d-%H%M%S')}_#{nomeTela}.browser.png"
	begin
    	page.save_screenshot scrshot_fullpath
	rescue Exception => e
		write_log :error, "Erro excecao=#{e} , gerando screenshot, filepath=$scrshot_fullpath"
    end
	return
end


def load_java_properties(properties_filename)
    properties = {}
    File.open(properties_filename, 'r') do |properties_file|
      properties_file.read.each_line do |line|
        line.strip!
        if (line[0] != ?# and line[0] != ?=)
          i = line.index('=')
          if (i)
            properties[line[0..i - 1].strip] = line[i + 1..-1].strip
          else
            properties[line] = ''
          end
        end
      end      
    end
    properties
end

def save_java_properties(properties_filename, hash_props)
#	write_log :debug, "save_java_properties P00, properties_filename=#{properties_filename}, hash_props=#{hash_props}"
	out_str=''
	hash_props.keys.each do |k|
		out_str=out_str+"#{k}=#{hash_props[k]}\n"
	end
	tmp_path = "#{get_automtmpdir}/temp.#{obtem_alphanum_guid}.properties" #2018Mar19 earlyam - estava gerando PERMISSION DENIED antes, por faltar desambiguador "obtem_alphanum_guid" no nome do arquivo temporario!
	File.open(tmp_path,'wt') { |f| f.puts out_str }
	FileUtils.mv tmp_path, properties_filename
#	write_log :debug, "save_java_properties retornando, properties_filename=#{properties_filename}, hash_props=#{hash_props}"
end


def out_system(cmdline)
	return `#{cmdline}` 


	tmpfname = "#{get_automtmpdir}/out_system_#{obtem_alphanum_guid}.txt"
	cmd_completo = "#{cmdline} >#{tmpfname} 2>&1}"
	system cmd_completo
	retval = File.read(tmpfname) if File.exist? tmpfname
	FileUtils.rm tmpfname if File.exist? tmpfname
	return retval
end

def create_new_file(fname, content = nil)
	retval = nil
	File.open(fname,File::WRONLY|File::CREAT|File::EXCL) { |f|
		if content
			f.write content
		end
		if block_given?
			retval = yield(f)
		end
	}
	return retval
end


def str_short_random(s_orig, lmax, preserve_words=nil)
	#TODO 2018Mai5 - nao remover caracteres que estejam no meio da palavra 'Rnd'
	preserve_words = preserve_words || Array.new
	falhar "str_short_random, parametro preserve_words=#{preserve_words} invalido" if preserve_words.class != Array

	s_new = s_orig
	#puts "check, s_orig=#{s_orig}, len=#{s_orig.length}"
	while s_new.length > lmax
		preserve_w_pos = []  #posicao de cada palavra que nao pode ser adulterada
		preserve_words.each do |w|
			i = s_new.index w
			w.length.times do |k|
				preserve_w_pos << i + k #todas as posicoes na String ocupadas pelas palavras a preservar. Exempo: s=Rnd1000020000BLABLABLAMrk25 , preserve_words=['Rnd','Mrk'], preserve_w_pos ficará [0,1,2,22,23,24], de forma a impedir que qualquer um dos caracteres com essas posições sejam removidos para reduzir comprimento da string.
			end
		end
		#puts "preserve_w_pos=#{preserve_w_pos}"

		p1=''
		p2=''
		pos_remove = nil
		visited=[] #posicoes ja visitadas. Se ja visitou todas , falha/raise 
		begin 
			if visited.length == s_new.length
				falhar "str_short_random, nao foi possivel reduzir palavra, s_orig=#{s_orig}, lmax=#{lmax}, preserve_words=#{preserve_words}, s_new=#{s_new}"  		
			end
			pos_remove = Random.new.rand(0..s_new.length-1) 
			visited << pos_remove if not visited.include?(pos_remove)
		end until not preserve_w_pos.include?(pos_remove) #loop até que tenha escolhido caracter a remover que nao esteja dentro das palavras a preservar
	
		#puts "pos_remove=#{pos_remove}"
		p1 = s_new[0..pos_remove-1] if pos_remove > 0
		#puts "p1=#{p1}"
		p2 = s_new[pos_remove+1..-1] if pos_remove < s_new.length - 1
		#puts "p2=#{p2}"
		s_new = "#{p1}#{p2}"
		#puts "agora, s_new =#{s_new}, len=#{s_new.length}"
		falhar 'Erro interno de implementacao' if s_new.length > s_orig.length 
	end
	s_new
end

