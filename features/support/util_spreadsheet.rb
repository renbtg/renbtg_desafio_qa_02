require_relative './log.rb'
require 'spreadsheet'
def ler_xls_com_id(caminho_excel, id_col_name=nil, id_col_value=nil, limite_linhas=1)
	#retorna a primeira linha (Hash: chave/valor, colname/colvalue) com coluna de nome id_col_name sendo igual id_col_value
	# NAO FUNCIONA para XLSX, apenas para XLS (Office 97/2003) - LE apenas primeira planilha do XLS
	#
	# SUGIRO copiar e colar este fonte para dentro de utils.rb, no diretorio support
	#
	#
	# limite_linhas: passar -1 para "ler todas linhas que atendam condicao"
	#
	#Excmplo de chamadas e uso:
=begin
	require_relative 'bla/bla/onde_colocou_este_metodo.rb'

	linha_hash = ler_xls_com_id('C:\Users\Santander\Documents\MASSA_IBPF\LOGIN_IBPF/massaIBPF_97.xls', 'TIPO CLI', 'JUR')
	if linha_hash == nil
		fail 'nao achei cpf na massa'
	else
		cpf_ou_cnpj = linha_hash['CPF OU CNPJ'] #nomes de coluna do XLS
		senha = linha_hash['SENHA'] #nomes de coluna do XLS

		#write_log :debug, "cpf_ou_cnpj=#{cpf_ou_cnpj}"
		#write_log :debug, "senha=#{senha}"
=end
	#write_log :debug, "EITA NOIS, caminho_excel=#{caminho_excel}"
	book = nil

	#write_log :debug, caminho_excel
	write_log :debug, "ler_xls_com_id, vai abrir arquivo, caminho_excel=#{caminho_excel}"
	book = Spreadsheet.open(caminho_excel)
	if book == nil
		#write_log :debug, "falha ao abrir arquivo #{caminho_excel}"
		fail "falha ao abrir arquivo #{caminho_excel}"
	end
	##puts  "ler_xls_com_id, DEBUG, ABRIU, caminho_excel=#{caminho_excel}"

	#write_log :debug, "ler_xls_com_id, abriu (ou tentou abrir) book, booj=#{book}"
	aba = book.worksheets.first # pega primeira planilha/aba do excel
	##puts  "ler_xls_com_id, DEBUG, obteve aba"
	all_rows=aba.rows
	##puts  "ler_xls_com_id, DEBUG, obteve all_rows"
	rzero = all_rows[0]
	##puts  "ler_xls_com_id, DEBUG, obteve rzero"
	ar_titulo =  rzero.to_a
	##puts  "ler_xls_com_id, DEBUG, obteve ar_titulo"
	tot_cols = ar_titulo.length
	##puts  "ler_xls_com_id, DEBUG, obteve tot_cols"
	retval = []

	if id_col_name != nil
		n_col_filtragem = ar_titulo.index(id_col_name)
		if n_col_filtragem == nil
			return nil # ATENCAO - nem achou coluna com nome igual a coluna de filtragem! retorna Hash vazio??? RUIM??
		end
	end

	##puts  "ler_xls_com_id, DEBUG, ANTES loop aba.each"

	ct_lin = 0
	aba.each do |row| # classe de "Sheet" parece ter metodo EACH q itera linha-a-linha
		#write_log :debug, "ler_xls_com_id - aba.each - dentro"
		#puts  "ler_xls_com_id, DEBUG, P00, DENTRO loop aba.each"
		if row[0].nil? # se primeira coluna vazia... acabou loop
			break
		end
		ct_lin = ct_lin + 1
		#puts  "ler_xls_com_id, DEBUG, P00, P01, ct_lin=#{ct_lin} loop aba.each"
		if ct_lin == 1
			next #ja lidamos com header/titulos/primeira linha acima!
		end
		#puts  "ler_xls_com_id, DEBUG, P00, P02, ct_lin=#{ct_lin} loop aba.each"
		ar_linha_atual = row.to_a.clone
		#write_log :debug, "ct_lin=#{ct_lin}, spreadsheet, ar_linha_atual=#{ar_linha_atual}"
		meu_hash = Hash.new #IMPORTANTE, ou falha pra MULTIPLAS LINHAS RETORNADAS!
		#puts  "ler_xls_com_id, DEBUG, P00, P03, ar_titulo=#{ar_titulo}"
		ar_titulo.each {|e| meu_hash[e]=nil }  #definimos quais sao as colunas do hash de retorno
		#puts  "ler_xls_com_id, DEBUG, P00, P04, meu_hash=#{meu_hash}"

		tot_cols.times do |num_c|
			vlr =  ar_linha_atual[num_c]
			#puts  "ler_xls_com_id, DEBUG, P00, P05, vlr=#{vlr}"
			if vlr.is_a?(Numeric)
				vlr = vlr.to_i.to_s
			end

			meu_hash [ ar_titulo[num_c] ] = (vlr ? vlr.clone : nil) #2017Set23, protegido de nil
		end #preenche cada chave com valor
		#puts  "ler_xls_com_id, DEBUG, P00, P06, meu_hash=#{meu_hash}"

		if id_col_name == nil || meu_hash[id_col_name] == id_col_value # ENCONTRADO O ID PEDIDO!
			if limite_linhas != -1
				#write_log :debug, "retornando meu_hash imediato" 
				retval = meu_hash # !! ACHOU! TERMONOU! RETORNA Hash!... UMA LINHA! NAO ARRAY!
				break
			else
				#write_log :debug, "adicionando meu_hash a retval" 
				retval << meu_hash
			end

		end
		#puts  "ler_xls_com_id, DEBUG, DENTRO E NO FIM DE LOOP aba"
	end # fim loop de ler linhas
	#puts  "ler_xls_com_id, DEBUG, FORA DE LOOP aba"

	book.io.close if book
	#raise "see me, retval=#{retval}"
	return retval	# nao achou chave buscada depois de ler todas linhas
end