require 'cucumber'
require 'selenium/webdriver'
require 'capybara/dsl'
require 'site_prism'
require 'faker'
require 'monetize'
require_relative './basic_utils.rb'


require_relative './log.rb'
require_relative './util_spreadsheet.rb'
require_relative './shared_utils.rb'

require 'selenium-webdriver'



def formata_cpf(str)
	return "#{str[0..2]}.#{str[3..5]}.#{str[6..8]}-#{str[9..10]}"
end

def formata_data(str)
	return "#{str[0..1]}/#{str[2..3]}/#{str[4..7]}"
end

def formata_cep(str)
	return "#{str[0..4]}-#{str[5..7]}"
end


def escreve_texto(el, str, pausas=nil)
	scroll_to(el) #2017Out3
	if false
		#2018Mar03 - abordagem anterior derrubava conexao do Selenium!
		el.set str 
		return
	end

	if not pausas
		s_pausas=(ENV['TEST_PAUSE_SECONDS']||'0/0.2/0.5') 
		pausas=s_pausas.split('/').map{|s|s.to_f}
	end
	write_log :debug, "escreve_texto: pausas=#{pausas}, str=#{str}"
	pausas.each_index do |i|
		pausa = pausas[i]
		if i > 0
			el.native.clear
		end
		if pausa==0
			el.set str
		else
			str.each_char do |c|
				sleep pausa
				el.native.send_keys c
			end
		end
		sleep 1
		conteudo = el.value
		if (conteudo == str) or (conteudo == formata_cpf(str)) or (conteudo == formata_data(str)) or (conteudo == formata_cep(str))
			return #deu certo
		end
		m_conteudo = Monetize.parse(conteudo)
		m_str = Monetize.parse(str)
		if (m_conteudo and m_str) and (m_conteudo.amount == m_str.amount or m_conteudo.amount == m_str.amount/100)
			return
		end

		loglevel = :warn
		loglevel=:error if i == pausas.length
		write_log loglevel, "escreve_texto: tentei escrever '#{str}', mas conteudo ficou '#{conteudo}'"
	end
	falhar "Erro em escreve_texto, conteudo diferente do parametro" 
end




def scroll_to(element, pAlignTop=false, options={:save_scrolled=>true}) #2017Nov17 00:48am, voltei pra implementacao original
	#2017Nov15 - alinha no TOPO da página. Aumenta chance de scroll default tornar elemento visivel e clicável!. Isso ajuda muito o webdesktop, que pode ser bastante minimizado verticalmente,
	# para < 30% do tamanho do desktop. Já o Mob é menos tolerante como minimizacao vertical: abaixo
	#de 55% ou 50%, já começa a ter erro "element not clickable at this point"! Percebtuais obtidos
	#no notenbook da RSI de Renato battaglia. CREIO que MOB funciona com 50% de minimização .
	#
	#   DETECTADO depois que alignTop é ruim pra MOB, apesar de bom pra WEBDESK. Alterado.. Entao: 
	# pra preservar comportamento original de "non-webdesktop", terá default FALSE sempre que
	# nao for webdesktop. 
	#
    return if not element #2017Out3 14:44pm BrTime


    
	alignTop = pAlignTop

	save_scrolled = false
	if true #qualapp == 'webdesktop'
		options = options || {:save_scrolled => true}
		save_scrolled = true
		if options[:save_scrolled] != nil
			save_scrolled = options[:save_scrolled]
		end
		if save_scrolled
			$last_scrolled_element = element
		end
	end

    write_log :debug, "scroll_to, pAlignTop=#{pAlignTop}, alignTop=#{alignTop}"
    script = <<-JS
      arguments[0].scrollIntoView(#{alignTop});
    JS
#2017Set9 - scrollIntoView(false)=avança o suficiente p/ elto todo ser visivel. Ou seja,
#Se true, a parte superior do elemento ficará alinhada com o topo da área visível do elemento-pai. Este é o valor default.
#Se false, a parte inferior do elemento ficará alinhada com o fundo da área visível do elemento-pai.	
#https://developer.mozilla.org/pt-BR/docs/Web/API/Element/scrollIntoView
	Capybara.current_session.driver.browser.execute_script(script, element.native)
end



def flock(file, mode, &block)
	success = file.flock(mode)
	if success
		begin
			block.call  file
		ensure
			file.flock(File::LOCK_UN)
		end
	end
	return success
end

def string_is_integer(str)
	return /\A[-+]?\d+\z/ === str
end


def executa_com_retry_stale_element
	begin
		yield
	rescue Selenium::WebDriver::Error::StaleElementReferenceError => e
		write_log :trace, 'StaleElementReferenceError executa_com_retry_stale_element, refazendo...'
		yield
	rescue Exception => e
		write_log :error, "Outra excecao, classe=#{e.class}, message=#{e.message} em executa_com_retry_stale_element, fazendo RAISE"
		raise e
	end
end




def get_nome_de_screenshot
	
	"reports/scrshot_#{get_alphanumcenario(Thread.current[:nome_cenario_corrente])}" 
end



def fecha_janela_browser
	run_fecha_janela_browser
end


def run_fecha_janela_browser() #parametro pra manipulacao em CONSOLE!

	write_log :debug, "run_fecha_janela_browser:  P00"

	Capybara.page.driver.quit
end

def log_chrome_port
	write_log :trace, "Porta chrome de Capybara.page.driver.BLA=#{get_porta_chromedriver}"
end
def get_porta_chromedriver
	ht=Capybara.page.driver.options[:http_client]
	ht.inspect.split('@server_url=').last.split(',').first.split(':').last.to_i
end

def abre_janela_browser()
	run_abre_janela_browser
	maximiza_este_browser
end

def run_abre_janela_browser(path_botao_err_chrome='.')
	core_abre_janela_browser
end

def core_abre_janela_browser
	begin
		#log_chrome_port
		Capybara.page.driver.quit
		#log_chrome_port
	rescue Exception => e
		#log_chrome_port
		write_log :debug, "Alguma excecao ignoravel no Capybara.page.driver.quit forçado que coloquei antes de todos visit c:, excecao=#{e}"
	end

	begin
		visit "file:///#{get_automdir}/autom.html"
	rescue Exception => e
		#retrying after some sleep...
		sleep 3
		begin
			Capybara.page.driver.quit #2018Set17 23:50, high hopes
		rescue Exception => e
			write_log :debug, "AGAIN Alguma excecao ignoravel no Capybara.page.driver.quit forçado que coloquei antes de todos visit c:, excecao=#{e}"
		end
		begin
			write_log :debug, "core_abre_janela_browser : retentando visit"
			#	log_chrome_port
			visit "file:///#{get_automdir}/autom.html"
			#	log_chrome_port
		rescue Exception => e
			#	log_chrome_port
			falhar "excecao=#{e},excecao manipulada VINDA DE VISIT DE core_abre_janela_browser - nao vou mandar corpo original pq pode ter acento e gerar CP850"
		end
	end
	write_log :debug, "core_abre_janela_browser -   retornando"
	#log_chrome_port

end

def str_to_xpath_contains(str, ponto_ou_text_class_etc='.', and_ou_or='and')
	badencode_webdesk = false #2018Jun14 - de true pra false
	if badencode_webdesk
		str = ascii_only_str(str).gsub('*',' ') #2017Dez04, GAMBIARRA para bad encoding em deployment de webdesktop
	end
	return strspace_to_xpath_fragment(str, 'contains', ponto_ou_text_class_etc, and_ou_or)
end

def strspace_to_xpath_fragment(str, funcao='contains', ponto_ou_text_class_etc='.', and_ou_or='and')
 	retval = ''

	a = str.split(' ')

	a.length.times do |i|
		if i > 0
			retval = retval + " #{and_ou_or.strip} "
		end
		retval = retval +  "#{funcao}(#{ponto_ou_text_class_etc},'#{a[i]}')"
	end
	retval =  "(#{retval})"
	return retval
end



def nbsp
	return "\u00a0" #2017Nov7
end

def get_airport_data
	ler_xls_com_id 'massas_feature/IATA airport codes.xls', nil, nil, -1
end
def get_customer_country_names
	(ler_xls_com_id 'massas_feature/phptravel_countries.xls', nil, nil, -1).map{|r| r['COUNTRY_NAME']}
end

def converte_massa_dinamica(raw_massa)
	ret = raw_massa.clone

	dyn_keys = raw_massa.keys.select{|k| raw_massa[k].downcase.start_with? '%dyn'}
	dyn_keys.each{|k|
		ret[k] = valor_massa_dinamica(raw_massa[k])
	}
	ret
end


def valor_massa_dinamica(c)
	ret = c
	dyn = c.split('%dyn').last.strip.downcase
	if dyn == 'airport_code'
		airports = get_airport_data.map{|r| r['Code']}
		i = Random.new.rand(0..airports.length-1)
		ret = airports[i]
	elsif dyn == 'customer_country'
		country_names = get_customer_country_names
		i = Random.new.rand(0..country_names.length-1)
		ret = country_names[i]
	elsif dyn.start_with? 'password' # PASSWORD 10=10 pos, PASSWORD 12=12 pos, PASSWORD=undef, default 15, PASSWORD_bla=invalid, will return PASSWORD_bla itself, PASSWORD 12 BLA=invalid, also echos back original content. PS:  > 64 (maxlen)=invalid, echoes original.
		maxlen = 64
		default_len = 15
		spl = dyn.split('password')
		len = nil
		if spl.length == 2 
			len = spl[1].to_i 
			if len > maxlen
				len = nil
			end
		else
			len = default_len
		end
		if len != nil
			ret = SecureRandom.base64(maxlen)[0..len-1]
		end
	elsif dyn.start_with? 'int_rand' 
		#int_rand ---> random integer, int rand 55 900 = betwen values 55 and 900
		spl = dyn.split('int_rand')

		if spl.length != 2
			#error
		else
			nums = spl[1].split(' ')
			low  = nums[0].to_i
			high = nums[1].to_i
			if low <= high
				ret = Random.new.rand(low..high)
			end
		end
	elsif dyn.start_with? 'near_hour' 
	 	#near_hour --> random datetime near now, between minus and plus hours. If Time.now()=2018-06-16 12:00:00, then "near_hour -2412 +35.5" will return value between '2018-06-06 00:00:00' (10 days and 12h before) and "2016-06-17 23:30:00" (1 day 11h 30mins later). An additional parameter, a base datetime, can be passed, like "int_rand -120 +45.5 2018-12-29T23:15:30". @@@ FRACTINARY HOURS used for dandomly adding and subtracting time, so, asking betwwen -1 hour and +1 hour can retudn a few seconds or minutes, not whole hours. 
	 	
		spl = dyn.split('near_hour')

		if not [2,3].include? spl.length
			#error
		else
			nums = spl[1].split(' ')
			low  = nums[0].to_f
			high = nums[1].to_f
			rnd = nil
			if low <= high
				rnd = Random.new.rand(low..high)
				basetime = nil
				if nums.length > 2
					basetime = Time.parse(nums[2])
				else
					basetime = Time.now()
				end
				seconds = rnd * 3600
				ret = basetime + seconds
			end
		end
	elsif dyn== 'near_hour'
		spl = dyn.split('near_hour')
		if spl.length != 2
			#error
		else
			nums = spl[1].split(' ')
			low  = nums[0].to_i
			high = nums[1].to_i
			if low <= high
				ret = Random.new.rand(low..high).to_s
			end
		end
	elsif dyn == 'fake city'
		ret = Faker::Address.city
	elsif dyn == 'fake zip'
		ret = Faker::Address.zip
	elsif dyn == 'fake country'
		ret = Faker::Address.country
	elsif dyn == 'fake last_name'
		ret = Faker::Name.last_name
	elsif dyn == 'fake first_name'
		ret = Faker::Name.first_name
	elsif dyn == 'fake mobile'
		ret = Faker::PhoneNumber.cell_phone
	elsif dyn == 'fake address1'
		ret = Faker::Address.street_address
	elsif dyn == 'fake address2'
		ret = Faker::Address.city + ' ' + Faker::Address.state_abbr
	elsif dyn == 'fake email'
		ret = Faker::Internet.safe_email
	end

	(ret||'c').to_s #safety. always returns string, and echoes parameter back if nil
end

def set_massa_cen(cen=nil)
	#Thread.current[:nome_cenario_corrente] = cen

	write_log "set_massa, P00, cen=#{cen}"
	cen = cen || "#{get_alphanumcenario(Thread.current[:nome_cenario_corrente])}"
	write_log "set_massa, P01, cen=#{cen}"
	dirs_xls = Dir["massas_feature/#{cen}*"]
	write_log "set_massa, P02, dirs_xls=#{dirs_xls}"
	um_dir = dirs_xls.first
	caminho_xls = "#{um_dir}/massa.xls"
	write_log :debug, "caminho_xls de massa=#{caminho_xls}"
	whole_massa = ler_xls_com_id(caminho_xls,nil,nil,-1)
	#write_log :debug, "whole_massa=#{whole_massa}"
	raw_massa = whole_massa.find {|o| ((o['UTILIZAR_MASSA']||'S').upcase)[0] != 'N'}
	raw_massa.keys.each{|k|
		raw_massa[k] = (raw_massa[k] || '').to_s #always string, never nil
	}

	$massa = converte_massa_dinamica(raw_massa)
	puts "$massa=#{$massa}" #mostra massa de dados em report do cucumber

	return
end


def delete_all_chrome_cookies
jscrp='
chromeUserData = "C:\Users\\" + Environment.UserName.ToString(CultureInfo.InvariantCulture) + "\\AppData\Local\Google\Chrome\User Data";
var chromeAdvancedSettings = "chrome://settings/clearBrowserData";
var options = new ChromeOptions();
options.AddArgument("--lang=en");
options.AddArgument("--user-data-dir=" + chromeUserData);
options.LeaveBrowserRunning = false;
var driver = new ChromeDriver(options);
driver.Navigate().GoToUrl(chromeAdvancedSettings);

var frame = driver.FindElement(By.XPath("//iframe[@src=\'chrome://settings-frame/clearBrowserData\']"));
var frameDriver = driver.SwitchTo().Frame(frame);
var dropDown = new SelectElement(frameDriver.FindElement(By.Id("clear-browser-data-time-period")));
dropDown.SelectByIndex(4);
var elm = driver.FindElement(By.Id("delete-cookies-checkbox"));
if (!elm.Selected) elm.Click();
elm = driver.FindElement(By.XPath("//button[@id=\'clear-browser-data-commit\']"));
elm.Click();
var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
waiter.Until(wd => wd.Url.StartsWith("chrome://settings"));
driver.Navigate().GoToUrl("chrome://newtab");
'

	script = <<-JS
		#{jscrp}
	JS

	puts "SCRIPT - PROX LINHAS"
	puts script
	raise 'DESNECESSARIO - ao fechar janela, cookies terão morridos, e nova tela de LOGIN aparece, sem crise'
end

def get_automdir
	Dir.pwd	
end

def maximiza_este_browser
	Capybara.page.driver.browser.manage.window.maximize #MAXIMIZE funcionou
end
