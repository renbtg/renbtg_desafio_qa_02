Quando(/^Navego para pagina de adicionar cliente$/) do
	ImcCustomersPage.new.process_goto_addcustomer
end

E(/^preencho dados do cliente$/) do
	ImcCustomersPage.new.process_fillin
end

Entao(/^submeto formulado de adicionar cliente com sucesso$/) do
	ImcCustomersPage.new.process_submit
end
