Dado(/^que visito o site$/) do
	ImcFlightsPage.new.process_start
end

Quando(/^preencho dados de pesquisa$/) do
	ImcFlightsPage.new.process_fill_data
end

Entao(/^busca retorna voos corretos$/) do
	ImcFlightsPage.new.process_search_and_validate_flights_table
end