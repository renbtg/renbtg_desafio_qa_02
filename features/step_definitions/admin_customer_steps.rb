Dado (/^que chamo servico com sucesso$/) do
	@ica = InmetricsCreateActivity.new
	@ica.call
end

E(/^verifico que status ok$/) do
	@ica.check_status_ok
end

Entao(/^garanto que request e response batem$/) do
	@ica.check_request_response_matches
end
