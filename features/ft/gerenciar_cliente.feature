#language: pt
#utf-8

Funcionalidade: Gerenciar cliente
Como tester, quero gerenciar cliente

Cenario: Adicionar cliente
Quando Abro pagina de login
E preencho usuario e senha
Entao submeto e concluo login com sucesso
Quando Navego para pagina de adicionar cliente
E preencho dados do cliente
Entao submeto formulado de adicionar cliente com sucesso

