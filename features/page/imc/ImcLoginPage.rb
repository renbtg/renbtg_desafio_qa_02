class ImcLoginPage < SitePrism::Page

#element :login_panel, :css, "h2",:text => 'Login Panel'
XPATH_CONTAINS_LOGIN_PANEL = str_to_xpath_contains("Login Panel")
#element :login_panel , :xpath, "//h2[text()[#{XPATH_CONTAINS_LOGIN_PANEL}]"
element :login_panel, :xpath,	"//h2[text()[#{XPATH_CONTAINS_LOGIN_PANEL}]]", visible:false
element :email_field,  :xpath, "//input[@type='text' and @name='email']", visible:false
element :password_field,  :xpath, "//input[@type='password' and @name='password']", visible:false
element :login_button, :xpath, "//button[@type='submit']", visible:false
element :dashboard_label, :css, 'a.dash'


#------------------------------------------------------------------------------------------------------------------------
	


	def process_start()
		remove_nonascii_exception {run_process_start}
	end
	def process_fillin
		remove_nonascii_exception {run_process_fillin}
	end

	def process_submit
		remove_nonascii_exception {run_process_submit}
	end

private
	def run_process_start
		visit 'https://www.phptravels.net/admin'
		if false #NOT SUPPORTED BY SELENIUM WEBDRIVER, must be tested in WAIT of some HTML
			if page.status_code != 200
				fail "Failed opening page, status code=#{status_code}"
			end
		end
		gera_screenshot "Just after visiting"

		executa_com_retry_stale_element {wait_for_login_panel 3*20}
		if not has_login_panel?
			fail "await_login_screen: element named login_panel not found"
		end
	end
	
	def run_process_fillin()
		scroll_to email_field
		escreve_texto email_field, $massa['EMAIL']
		scroll_to password_field
		escreve_texto password_field, $massa['PASSWORD']

		gera_screenshot "after filling email and passowrd, will click login"
	end

	def run_process_submit()
		scroll_to login_button
		login_button.click
		executa_com_retry_stale_element {wait_for_dashboard_label 3*20}
		if not has_dashboard_label?
			fail "element dashboard_label not found, login failed"
		end
	end


end