require 'site_prism/table'
class ImcFlightsPage < SitePrism::Page
extend SitePrism::Table

$departure_date = nil
private

element :clickable_origin, :css, '#s2id_location_from'
element :clickable_destination, :css, '#s2id_location_to'

element :airport_origin_or_destination_input, 'div.select2-search input'

element :any_match, :xpath, "(//span[contains(@class,'select2-match')])[1]"

element :clickable_depart_date, :xpath, "//span[text()='Depart']" #

element :depart_date_input, :xpath, "//input[@placeholder='Depart']", visible:false

element :search_button, :xpath, "//button[@type='submit' and normalize-space(text())='Search']"

element :html_available_flights, 'table#load_data' 

table :available_flights_table, '#load_data' do 
	column :all_the_stuff
end 

element :first_row_of_flights, :xpath, "(//table[@id='load_data']/*/tr)[1]" 




public
#------------------------------------------------------------------------------------------------------------------------

	def process_search_and_validate_flights_table
		remove_nonascii_exception {run_search_and_validate_flights_table} 
	end

	def process_fill_data
		remove_nonascii_exception {run_process_fill_data}
	end


	def process_start()
		remove_nonascii_exception {run_process_start}
	end


private #deve ser private
	def run_process_fill_data
		fill_airport $massa['ORIGIN_AIRPORT'], 'origin', clickable_origin
		fill_airport $massa['DESTINATION_AIRPORT'], 'destination', clickable_destination
		escreve_texto depart_date_input, departure_date
	end


	def run_process_start
		visit 'https://www.phptravels.net/flights'
		if false #NOT SUPPORTED BY SELENIUM WEBDRIVER, must be tested in WAIT of some HTML
			if page.status_code != 200
				fail "Failed opening page, status code=#{status_code}"
			end
		end
		gera_screenshot "Just after visiting"

		#not checking for basic due page completed
		
	end

	def run_search_and_validate_flights_table
		search_button.click
		validate_flights_table		
	end

	def validate_flights_table
		#table present, as rows, and and at least first row contains data duely filtered

		wait_for_html_available_flights 120
		if not has_html_available_flights?
			fail 'Flights table not found'
		end
		rows = available_flights_table.rows
		if (not rows) or (rows.length == 0)
			fail 'Search returned no rows in flights table'
		end
		scroll_to first_row_of_flights, true		
		r = rows.first[:all_the_stuff].split("\n").map{|s|s.strip}
		r.each_index{|i| write_log :debug, "@@row=[[#{i}]], @@text=[[#{r[i]}]]" }


		colmap = {:origin_airport_initials => 5,			:destination_airport_initials => 6,			:departure_time_and_date => 7,			:origin_airport_name => 21,			:destination_airport_name => 23		}


		if (not r[colmap[:origin_airport_initials]].downcase.include? $massa['ORIGIN_AIRPORT'].downcase) and (not r[colmap[:origin_airport_name]].downcase.include? $massa['ORIGIN_AIRPORT'].downcase)
			fail "Origin airport in first row of available flights table does not match test data - table row value INITIALS=#{r[colmap[:origin_airport_initials]]}, table row value NAME=#{r[colmap[:origin_airport_name]]}, test data=#{$massa['ORIGIN_AIRPORT']}"
		end

		if (not r[colmap[:destination_airport_initials]].downcase.include? $massa['DESTINATION_AIRPORT'].downcase) and (not r[colmap[:destination_airport_name]].downcase.include? $massa['DESTINATION_AIRPORT'].downcase)
			fail "Destination airport in first row of available flights table does not match test data - table row value INITIALS=#{r[colmap[:destination_airport_initials]]}, table row value NAME=#{r[colmap[:destination_airport_name]]}, test data=#{$massa['DESTINATION_AIRPORT']}"
		end

		if not r[colmap[:departure_time_and_date]].downcase.include? departure_date
			fail "Departure date in first row of available flights table does not match test data - table row value =#{r[colmap[:departure_time_and_date]]}, , test data(calculated)=#{departure_date}"
		end
		write_log :debug, "run_validate_flights_table, showing off programming skills"
	end

	def fill_airport(s_airport, s_description, el_origin_or_destination)
		el_origin_or_destination.click
		el = airport_origin_or_destination_input
		escreve_texto el, s_airport
		wait_for_any_match 30
		if not has_any_match?
			fail "no match found for #{s_description} _airport=#{s_airport}"
		end
		any_match.click	
		#airport_origin_or_destination_input.native.send_keys :enter
	end


	def days_in_month(month, year)
		common_year_days_in_month = [nil, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

		return 29 if month == 2 && Date.gregorian_leap?(year)
		common_year_days_in_month[month]
	end

	def departure_date
		return $departure_date if $departure_date

		init = DateTime.now
		#init = DateTime.now << 36
		hiseason_months = [1,2,7,12]
		months_ahead = 3


		t = Time.parse((init >> months_ahead).to_s)
		while hiseason_months.include? t.month
			t = Time.parse( ((DateTime.parse t.to_s) >> 1).to_s )
		end
		max_day = days_in_month(t.month, t.year)
		new_day =  Random.new.rand(1..max_day)
		t=Time.mktime(t.year, t.month, new_day)
		
		$departure_date = t.strftime("%Y-%m-%d")
		$departure_date
	end




end