require 'site_prism/table'
class ImcCustomersPage < SitePrism::Page
extend SitePrism::Table

private

XPATH_CONTAINS_CUSTOMERS_MGMT = str_to_xpath_contains("Customers Management")
XPATH_CONTAINS_ADD = str_to_xpath_contains("Add")
XPATH_CONTAINS_ADD_CUSTOMER = str_to_xpath_contains("Add")




element :accounts_menu,  :xpath, "//a[@href='#ACCOUNTS' and not(@aria-expanded='true')]"
element :expanded_accounts_menu,  :xpath, "//a[@href='#ACCOUNTS' and @aria-expanded='true']"
element :customers_menu,  :xpath, "//a[text()='Customers']"
element :customers_management_label, :xpath, "//div[text()[#{XPATH_CONTAINS_CUSTOMERS_MGMT}]]"
element :add_customer_button, :xpath, "//button[text()[#{XPATH_CONTAINS_ADD}]]"
element :add_customer_label, :xpath,	"//div[text()[#{XPATH_CONTAINS_ADD_CUSTOMER}]]"
element :first_name_field,  :xpath, "//input[@type='text' and @name='fname']", visible:false
element :last_name_field,  :xpath, "//input[@type='text' and @name='lname']", visible:false
element :email_field,  :xpath, "//input[@type='email' and @name='email']", visible:false
element :password_field,  :xpath, "//input[@type='password' and @name='password']", visible:false
element :mobile_field,  :xpath, "//input[@type='text' and @name='mobile']", visible:false
element :country_field, :xpath, "//select[@name='country']", visible:false #ImcCustomersPage.new.country_field.find(:option,'Brazil').select_option
element :address1_field,  :xpath, "//input[@type='text' and @name='address1']", visible:false
element :address2_field,  :xpath, "//input[@type='text' and @name='address2']", visible:false
element :status_field, :xpath, "//select[@name='status']", visible:false 
element :submit_button, :xpath, "//button[text()='Submit']", visible:false

table :customers_table, :xpath, "//table[contains(@class,'xcrud-list')]" do 
	column :marked
	column :rownum
	column :first_name
	column :last_name
	column :email
	column :last_login
	column :active
	column :actions
end

public
#------------------------------------------------------------------------------------------------------------------------
	

	def process_goto_addcustomer()
		remove_nonascii_exception {run_process_goto_addcustomer}
	end
	def process_fillin()
		remove_nonascii_exception {run_process_fillin}
	end
	def process_submit()
		remove_nonascii_exception {run_process_submit}
	end

private
	def run_process_goto_addcustomer()
		#

		gera_screenshot "Must be in DASHBOARD, having ACCOUNTS"
		wait_for_accounts_menu 60
		if not has_accounts_menu?
			fail 'element accounts_menu not found, not in correct DASHBOARD screen, or problem in DASHBOARD screen'
		end
		accounts_menu.click
		wait_for_expanded_accounts_menu 20
		if not has_customers_menu?
			fail "element customers_menu not found within expanded ACCOUNTS meny"
		end
		customers_menu.click
		wait_for_customers_management_label 60
		if not has_customers_management_label?
			fail "element customers_management_label not found, customer mgmt. screen not properly shown"
		end

		assure_customer_notthere

		add_customer_button.click
		wait_for_add_customer_label 60
		if not has_add_customer_label?
			fail "element add_customer_label not found, Add Customer screen not properly shwon"
		end

	end


	def run_process_fillin
		scroll_to first_name_field; escreve_texto first_name_field, $massa['FIRST_NAME']
		scroll_to last_name_field; escreve_texto last_name_field, $massa['LAST_NAME']
		scroll_to email_field; escreve_texto email_field, $massa['CUST_EMAIL']
		scroll_to password_field; escreve_texto password_field, $massa['CUST_PASSWORD']
		scroll_to mobile_field; escreve_texto mobile_field, $massa['MOBILE']
		scroll_to country_field; country_field.find(:option,$massa['COUNTRY']).select_option
		scroll_to address1_field; escreve_texto address1_field, $massa['ADDRESS1']
		scroll_to address2_field; escreve_texto address2_field, $massa['ADDRESS2']
		if $massa['ENAB>ED'] == '1'
			cust_status = 'Enabled'
		else
			cust_status = 'Disabled'
		end
		
		scroll_to status_field; status_field.find(:option,cust_status).select_option
	end


	def run_process_submit
		scroll_to submit_button; submit_button.click

		wait_for_customers_management_label 60
		if not has_customers_management_label?
			fail "Failed returning to customer mgmt screen, element customers_management_label not found, some problem after submitting new customer"
		end

		write_log :debug, "sleeping arbitrariy seconds, giving UI time to fully present customers table, how could I tell table has fully loaded? Do i need this sleep?"
		sleep 15
		write_log :debug, "done sleeping arbitrariy , gave UI time to fully present customers table"

		assure_customer_there

		write_log :debug, 'SUCCESSFULLY added customer'
	end

	def assure_customer_notthere
		cust_array = customers_table.rows
		if cust_array.any?{|c| c[:email] == $massa['CUST_EMAIL']}
			fail "element customers_table already contains email to add, #{$massa['CUST_EMAIL']}"
		end
	end
	def assure_customer_there
		cust_array = customers_table.rows
		if not cust_array.any?{|c| c[:email] == $massa['CUST_EMAIL']}
			fail "email of added customer not found in element customers_table, #{$massa['CUST_EMAIL']}, not successfully added"
		end
	end

	def await_login_screen
		executa_com_retry_stale_element {wait_for_login_panel 3*20}
		if not has_login_panel?
			fail "await_login_screen: element named login_panel not found"
		end

	end

end